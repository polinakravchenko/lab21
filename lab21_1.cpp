﻿// lab21_1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

template <typename T, int size>
class StaticArray
{
private:
   
    T m_array[size]{};

public:
    T* getArray();

    T& operator[](int index)
    {
        return m_array[index];
    }
};


template <typename T, int size>
T* StaticArray<T, size>::getArray()
{
    return m_array;
}

int main()
{
    
    StaticArray<int, 10> intArray;

    // Fill it up in order, then print it backwards
    for (int count{ 0 }; count < 10; ++count)
        intArray[count] = count;

    for (int count{ 9 }; count >= 0; --count)
        std::cout << intArray[count] << ' ';
    std::cout << '\n';

    return 0;
}