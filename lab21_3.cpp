﻿// lab21_3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

using namespace std;

class Number
{
private:
    int d;

public:
    Number(int _d) : d(_d) { }

    int Get() { return d; }
    void Set(int _d)
    {
        d = _d;
    }

    void Print(string msg)
    {
        cout << msg.c_str() << d << endl;
    }
};

template <typename T> // шаблонний клас з вказівкою

class SmartPtr
{
private:
    T* p; 
    int count; 

public:
  
    SmartPtr(T* _p = nullptr)
    {
     
        count = 0;
        p = _p;
    }
    SmartPtr(const SmartPtr& obj)
    {
        p = obj.p;
        count++;
    }
    SmartPtr operator=(const SmartPtr& obj)
    {
        p = obj.p;
        count++;
        return *this;
    }
    ~SmartPtr()
    {
        if ((p != nullptr) && (count == 0))
        {
            cout << "Delete object" << endl;
            delete[] p;
        }
        else
        {
            cout << "Delete copy" << endl;
            count--; 
        }
    }
    T* operator->()
    {
        return p;
    }
};

void main()
{

    Number* obj1 = new Number(10);
    obj1->Print("obj1: ");

    SmartPtr<Number> ptr(obj1);
    ptr->Print("ptr->obj: ");

    SmartPtr<Number> ptr2 = ptr; 
    ptr2->Print("ptr2->obj: ");

    SmartPtr<Number> ptr3;
    ptr3 = ptr2; 
    ptr3->Print("ptr3->obj: ");
}
